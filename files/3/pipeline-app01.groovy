pipeline {
    agent any

    environment { 
        registry = "lecioneto/app01" 
        registryCredential = 'dockerhub_id' 
        dockerImage = ''
    }

    stages {
        stage('Clone Repository') {
            steps {
                git url: 'https://gitlab.com/lecioneto/app01.git'
            }
        }
        stage('Build Docker Image') {
            steps {
                script {
                    dockerImage = docker.build registry + ":develop"
                }
            }
        }
        stage('Send image to Docker Hub') {
            steps {
                script {
                    docker.withRegistry( '', registryCredential ) { 
                    dockerImage.push() 
                    }
                }
            }
        }
    	stage('Deploy') {
		    steps{
                    step([$class: 'AWSCodeDeployPublisher', 
                        applicationName: 'app01-application',
                        awsAccessKey: "AKIAJQK7HQDM5HIZXY5Q",
                        awsSecretKey: "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",
                        credentials: 'awsAccessKey',
                        deploymentGroupAppspec: false,
                        deploymentGroupName: 'service',
                        deploymentMethod: 'deploy',
                        excludes: '',
                        iamRoleArn: '',
                        includes: '**',
                        pollingFreqSec: 15,
                        pollingTimeoutSec: 600,
                        proxyHost: '',
                        proxyPort: 0,
                        region: 'us-east-1',
                        s3bucket: 'app01-lsn', 
                        s3prefix: '', 
                        subdirectory: '',
                        versionFileName: '',
                        waitForCompletion: true])
            }
        }
        stage('Cleaning up') {
            steps {
                sh "docker rmi $registry:develop"
            }
        }
    }
}