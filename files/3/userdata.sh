#!/bin/bash

sudo yum update -y
sudo yum install -y aws-cli ruby wget
sudo amazon-linux-extras install docker
sudo usermod -aG docker ec2-user
sudo systemctl enable --now docker

# Install CodeDeploy Agent
wget https://aws-codedeploy-us-east-1.s3.us-east-1.amazonaws.com/latest/install
chmod +x ./install
sudo ./install auto
sudo service codedeploy-agent start
sudo chmod 777 /etc/init.d/codedeploy-agent